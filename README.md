# MAODV Event Driven Dynamic Path Oprtmization

Pius Pambudi (05111540000069) - Jaringan Nirkabel

## Penjelasan
Secara garis besar, setiap ada tetangga baru yang masuk, cari tahu apakah dia punya rute menuju destination yang lebih pendek.

1. Saat menemukan tetangga baru, kirim PRREQ (modifikasi RREQ).
2. Jika tetangga punya rute ke destination yang lebih pendek, balas dengan PRREP (modifikasi RREP) dan forward request ke destination.
3. Jika rute yang lebih pendek ditemukan, update routing table.

## Modifikasi
1. `recvHello`
2. `sendRequest`
3. `recvRequest`

## Paper
https://ieeexplore.ieee.org/document/7065524